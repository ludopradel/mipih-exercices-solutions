public class RomanNumeral {

    int[] arabics = {1000, 900, 500, 100, 10, 5, 4, 1};
    String[] romans = {"M", "CM", "D", "C", "X", "V", "IV", "I"};

    public String convert(int arabicNumber) {
        String romanNumber = "";

        for (int i = 0; i < romans.length; i++) {
            while (arabicNumber >= arabics[i]) {
                romanNumber += romans[i];
                arabicNumber -= arabics[i];
            }
        }

        return romanNumber;
    }

}
