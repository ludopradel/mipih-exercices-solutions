public class BowlingGame {

    private int[] rolls = new int[21];
    private int rollNumber = 0;

    public void roll(int pinsDown) {
        rolls[rollNumber++] = pinsDown;
    }

    public int score() {
        int score = 0;
        int rollPosition = 0;
        for (int frame = 0; frame < 10; frame++) {
            if (isSpare(rollPosition)) {
                score += spareScore(rollPosition);
                rollPosition = rollPosition + 2;
            } else if (isStrike(rollPosition)) {
                score += strikeScore(rollPosition);
                rollPosition = rollPosition + 1;
            } else {
                score += rolls[rollPosition] + rolls[rollPosition + 1];
                rollPosition = rollPosition + 2;
            }
        }
        return score;
    }

    private int strikeScore(int rollPosition) {
        return 10 + rolls[rollPosition + 1] + rolls[rollPosition + 2];
    }

    private int spareScore(int rollPosition) {
        return 10 + rolls[rollPosition + 2];
    }

    private boolean isStrike(int rollPosition) {
        return rolls[rollPosition] == 10;
    }

    private boolean isSpare(int rollPosition) {
        return rolls[rollPosition] + rolls[rollPosition + 1] == 10;
    }
}
