import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RomanNumeralTest {

    @Test
    public void should_convert_1_to_I() {
        //GIVEN
        RomanNumeral roman = new RomanNumeral();

        //WHEN
        String value = roman.convert(1);

        //THEN
        assertEquals("I", value);
    }

    @Test
    public void should_convert_2_to_II() {
        //GIVEN
        RomanNumeral roman = new RomanNumeral();

        //WHEN
        String value = roman.convert(2);

        //THEN
        assertEquals("II", value);
    }

    @Test
    public void should_convert_3_to_III() {
        //GIVEN
        RomanNumeral roman = new RomanNumeral();

        //WHEN
        String value = roman.convert(3);

        //THEN
        assertEquals("III", value);
    }

    @Test
    public void should_convert_10_to_X() {
        //GIVEN
        RomanNumeral roman = new RomanNumeral();

        //WHEN
        String value = roman.convert(10);

        //THEN
        assertEquals("X", value);
    }

    @Test
    public void should_convert_20_to_XX() {
        //GIVEN
        RomanNumeral roman = new RomanNumeral();

        //WHEN
        String value = roman.convert(20);

        //THEN
        assertEquals("XX", value);
    }

    @Test
    public void should_convert_200_to_CC() {
        //GIVEN
        RomanNumeral roman = new RomanNumeral();

        //WHEN
        String value = roman.convert(200);

        //THEN
        assertEquals("CC", value);
    }

    @Test
    public void should_convert_5_to_V() {
        //GIVEN
        RomanNumeral roman = new RomanNumeral();

        //WHEN
        String value = roman.convert(5);

        //THEN
        assertEquals("V", value);
    }

    @Test
    public void should_convert_4_to_IV() {
        //GIVEN
        RomanNumeral roman = new RomanNumeral();

        //WHEN
        String value = roman.convert(4);

        //THEN
        assertEquals("IV", value);
    }

    @Test
    public void should_convert_234_to_CCXXXIV() {
        //GIVEN
        RomanNumeral roman = new RomanNumeral();

        //WHEN
        String value = roman.convert(234);

        //THEN
        assertEquals("CCXXXIV", value);
    }

    @Test
    public void should_convert_1900_to_MCM() {
        //GIVEN
        RomanNumeral roman = new RomanNumeral();

        //WHEN
        String value = roman.convert(1900);

        //THEN
        assertEquals("MCM", value);
    }

    @Test
    public void should_convert_1500_to_MD() {
        //GIVEN
        RomanNumeral roman = new RomanNumeral();

        //WHEN
        String value = roman.convert(1500);

        //THEN
        assertEquals("MD", value);
    }





}
