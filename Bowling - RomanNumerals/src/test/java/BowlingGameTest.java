import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class BowlingGameTest {

    BowlingGame game;

    @Before
    public void setUp() {
        game = new BowlingGame();
    }

    @Test
    public void should_score_0_when_no_pin_down() {
        // GIVEN
        rollMany(20, 0);

        // WHEN
        int score = game.score();

        // THEN
        assertThat(score).isEqualTo(0);
    }


    @Test
    public void should_score_number_of_pins_down() {
        // GIVEN
        rollMany(1, 4);
        rollMany(19, 0);

        // WHEN
        int score = game.score();

        // THEN
        assertThat(score).isEqualTo(4);

    }

    @Test
    public void should_score_spare() {
        // GIVEN
        rollSpare();
        rollMany(1, 4);
        rollMany(17, 0);

        // WHEN
        int score = game.score();

        // THEN
        assertThat(score).isEqualTo(18);
    }

    @Test
    public void should_score_strike() {
        // GIVEN
        rollStrike();
        rollMany(1, 4);
        rollMany(1, 2);
        rollMany(16, 0);

        // WHEN
        int score = game.score();

        // THEN
        assertThat(score).isEqualTo(22);
    }

    @Test
    public void should_score_perfect_game() {
        // GIVEN
        rollMany(12, 10);

        // WHEN
        int score = game.score();

        // THEN
        assertThat(score).isEqualTo(300);
    }

    private void rollStrike() {
        rollMany(1, 10);
    }


    private void rollSpare() {
        rollMany(1, 3);
        rollMany(1, 7);
    }

    private void rollMany(int rollNumber, int pinsDown) {
        for (int i = 0; i < rollNumber; i++) {
            game.roll(pinsDown);
        }
    }

}
