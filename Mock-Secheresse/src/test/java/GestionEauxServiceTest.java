import dependencies.EtatService;
import dependencies.InfoSecheresseService;
import dependencies.MunicipalServices;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class GestionEauxServiceTest {

    @Test
    public void should_send_restriction_eau_when_secheresse_more_than_10_days() {

        // GIVEN
        EtatService dummyEtatService = Mockito.mock(EtatService.class);
        MunicipalServices mockMunicipalService = Mockito.mock(MunicipalServices.class);
        InfoSecheresseService stubInfoSecheresseService = Mockito.mock(InfoSecheresseService.class);

        Mockito.when(stubInfoSecheresseService.previsionDureeSecheresse()).thenReturn(11);

        GestionEauxService gestionEau = new GestionEauxService(stubInfoSecheresseService, dummyEtatService, mockMunicipalService);

        // WHEN
        gestionEau.checkEtatSecheresse();

        // THEN
        Mockito.verify(mockMunicipalService).sendRestrictionEauInformation();
    }

    @Test
    public void should_not_send_restriction_eau_when_secheresse_less_than_10_days() {

        // GIVEN
        EtatService dummyEtatService = Mockito.mock(EtatService.class);
        MunicipalServices mockMunicipalService = Mockito.spy(MunicipalServices.class);
        InfoSecheresseService stubInfoSecheresseService = Mockito.mock(InfoSecheresseService.class);

        Mockito.when(stubInfoSecheresseService.previsionDureeSecheresse()).thenReturn(6);

        GestionEauxService gestionEau = new GestionEauxService(stubInfoSecheresseService, dummyEtatService, mockMunicipalService);

        // WHEN
        gestionEau.checkEtatSecheresse();

        // THEN
        Mockito.verify(mockMunicipalService, Mockito.never()).sendRestrictionEauInformation();
    }

}