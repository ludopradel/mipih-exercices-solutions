package dependencies;

public interface InfoSecheresseService {
    /**
     * retourne une prévision du nombre de jours sans pluie à venir
     */
    public int previsionDureeSecheresse();

    /**
     * retourne l'état des réserves d'eau potable du village (en m3)
     */
    public double reserveEauMunicipale();
}