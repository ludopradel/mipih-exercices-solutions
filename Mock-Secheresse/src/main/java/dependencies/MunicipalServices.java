package dependencies;

import exceptions.CiterneVideException;

public interface MunicipalServices {
    /**
     * Envoie un message d'alerte aux habitants
     * pour indiquer les restrictions d'eau en cours
     */
    public void sendRestrictionEauInformation();

    /**
     * Appelle un service de livraison d'eau par camion citernes
     * @throws CiterneVideException lorsque les citernes ne peuvent plus livrer d'eau
     */
    public void callLivraisonCiterneEau() throws CiterneVideException;
}
