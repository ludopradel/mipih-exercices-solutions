import dependencies.EtatService;
import dependencies.InfoSecheresseService;
import dependencies.MunicipalServices;

public class GestionEauxService {

    private InfoSecheresseService infoSecheresse;
    private EtatService serviceEtat;
    private MunicipalServices servicesMunicipaux;

    GestionEauxService(InfoSecheresseService infoSecheresse,
                       EtatService serviceEtat,
                       MunicipalServices servicesMunicipaux) {

        this.infoSecheresse = infoSecheresse;
        this.serviceEtat = serviceEtat;
        this.servicesMunicipaux = servicesMunicipaux;
    }
    public void checkEtatSecheresse() {
        if (infoSecheresse.previsionDureeSecheresse() > 10) {
            servicesMunicipaux.sendRestrictionEauInformation();
        }
    }

}
